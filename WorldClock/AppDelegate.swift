//
//  AppDelegate.swift
//  WorldClock
//
//  Created by Jason Lenny on 12/06/2018.
//  Copyright © 2018 Jason Lenny. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    @IBOutlet weak var window: NSWindow!
    
    var statusBar = NSStatusBar.system
    var statusBarItem : NSStatusItem = NSStatusItem()
    
    var timeFormatter = DateFormatter()
    var dateFormatter = DateFormatter()
    
    let menu = NSMenu()
    
    let dateOption = NSMenuItem(title: "", action: #selector(AppDelegate.donothing), keyEquivalent: "")
    let quitOption = NSMenuItem(title: "Quit", action: #selector(AppDelegate.terminate), keyEquivalent: "q")
    
    // utility function for menu to quit
    @objc func terminate() {
        exit(0)
    }
    
    @objc func donothing() {
        // do nothing
    }
    
    var timer = Timer()
    
    @objc func timerAction() {
        let date = Date()
        
        timeFormatter.timeZone = TimeZone(identifier: "America/Los_Angeles")
        let pacific_time = timeFormatter.string(from:date)
        
        timeFormatter.timeZone = TimeZone(identifier: "America/New_York")
        let eastern_time = timeFormatter.string(from:date)
        
        timeFormatter.timeZone = TimeZone(identifier: "Europe/Amsterdam")
        let amsterdam_time = timeFormatter.string(from:date)
        
        timeFormatter.timeZone = TimeZone(identifier: "Asia/Kolkata")
        let newdelhi_time = timeFormatter.string(from:date)
        
        let display_date = dateFormatter.string(from:date)
        
        dateOption.title = display_date
        
        statusBarItem.button?.title = "🇺🇸" + pacific_time + " 🇺🇸" + eastern_time + " 🇳🇱" + amsterdam_time + " 🇮🇳" + newdelhi_time
    }
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        statusBarItem = statusBar.statusItem(withLength: -1)
        
        menu.addItem(quitOption)
        menu.addItem(dateOption)
        
        statusBarItem.menu = menu
        
        timeFormatter.dateFormat = "HH:mm"
        dateFormatter.dateFormat = "EEEE, MMM d yyyy"
        
        // Populate initial timer
        self.timerAction()
        
        // Synch to minute changeover
        let now = Date.timeIntervalSinceReferenceDate
        let delayFraction = trunc(now) - now
        
        //Caluclate a delay until the next even minute
        let delay = 60.0 - Double(Int(now) % 60) + delayFraction
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
            // update for this new minute and then start repeating
            self.timerAction()
            self.timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(AppDelegate.timerAction), userInfo: nil, repeats: true)
        })
        
        
    
    }
    
}
